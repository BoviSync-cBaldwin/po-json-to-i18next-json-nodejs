const fs = require('fs');
const _ = require('lodash');

/**
 * The source translations that will create the JSON
 * structure for the returned JSON.
 **/
const i18nextJson = require(process.argv[2]);
/**
 * The source of the translations being
 * fetched. Basically, the existing Bovisync
 * .po files as a JSON object
 **/
const oldT9ns = require(process.argv[3]);

let output = {};

/**
 * Traverses the i18nextJson structure, recurses when an
 * object is found in the JSON. If a string is found, it
 * search the oldT9ns JSON for a string that matches and
 * creates the path and the value on the output object.
 */
const navigateObj = (value, path) => {
  // If object, recurse until a value is found
  if( _.isObjectLike(value) ){
    _.forIn(value, (value, key) => {
      navigateObj(value, `${path}.${_.replace(key, '.', '_')}`);
    });
  }
  // If a string is found, go get the existing translation
  else if( _.isString(value) ){
    let context = getContextName(path);
    let string = searchObject(oldT9ns, `${context}${value}`); 
    if( _.isString(string) ){
      _.set(output, path, string);
    }
  }
}

/**
 * Uses the passed search term to find the existing
 * translation value in the oldT9ns JSON and returns it.
 */
const searchObject = (object, searchTerm) => {
  let result = null;
  let term = searchTerm;

  _.forIn(object, (value, key) => {
    if( key.indexOf(term) >= 0 ){
      result = value;
    }
    else if( _.isObjectLike(value) ){
      return searchObject(value, term);
    }
  });

  return result;
}

/**
 * This is a fancy way of getting the old gettext
 * context from the new translations namespaces.
 * It maps a namespace to a gettext context.
 *
 * Additional mappings will be need for other files.
 */
const getContextName = (name) => {
  let mod = name.split('.');
  let contextName = `${mod[1]}`;
  let returnVal = '';

  switch(contextName) {
    case 'rptItem':
      // This is a way to drive into a nested namespace, ugly but gets the job done.
      switch(mod[2]){
        case 'name':
          returnVal = 'rpt_name||';
          break;
        case 'shortName':
          returnVal = 'rpt_short||';
          break;
        case 'description':
          returnVal = 'zzz||';
          break;
      }
      break;
    case 'eventChoice':
      returnVal = 'ev_choice||';
      break;
    case 'eventField':
      returnVal = 'ev_field||';
      break;
    case 'evtItem':
      switch(mod[2]){
        case 'name':
          returnVal = 'ev_name||';
          break;
        case 'shortName':
          returnVal = 'ev_short||';
          break;
      }
      break;
  }

  return returnVal;
}

navigateObj(i18nextJson, 'top');
// print output to standard output. Then you can pipe it into less or print to file, etc.
console.log(JSON.stringify(output.top, null, 4));
